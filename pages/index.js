import { Inter } from '@next/font/google'
import FS from '@isomorphic-git/lightning-fs'
import * as git from 'isomorphic-git'
import http from "isomorphic-git/http/web"

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <div>
      <button onClick={executeTest}>Test</button>
    </div>
  )
}
async function executeTest() {
  const repo_name = '/test'
  const remote_url = '<<some repo url>>'
  const password = '<<repo access key>>'
  const name = '<name>'
  const email = '<<email>>'
  let fs = new FS(('/local_repo'))
  git.clone({
    fs,
    http,
    dir: repo_name,
    gitdir: repo_name,
    ref: 'main',
    remote: 'origin',
    url: remote_url,
    singleBranch: true,
    onAuth: () => ({ username: email, password: password }),
    onProgress: (message) => { console.log("progress", message) },
    onMessage: (message) => { console.log("message", message) },
    onAuthFailure: (err) => { console.log("auth failure", err) },
  })
    .then(async () => {
      await git.setConfig({
        fs,
        dir: repo_name,
        gitdir: repo_name,
        path: 'user.name',
        value: email,
        path: 'remote.origin.url',
        value: remote_url
      })
    })
    .then(async () => {
      let dirName = "/test_" + Math.round(Math.random() * 9999)
      await makeDir(repo_name, dirName)
      await fs.promises.writeFile(`${repo_name}${dirName}/.gitkeep`, `creating a dummy file with ${Math.random()}`)
      await git.add({ fs, gitdir:repo_name, dir: `${repo_name}${dirName}`, filepath: `.gitkeep` })
      console.log('added the file')
      let commit_payload = {
        fs,
        gitdir: repo_name,
        dir: dirName,
        ref: 'main',
        author: {
          name,
          email
        },
        message: `adding .gitkeep at ${new Date()}`
      }
      await git.commit(commit_payload)
      console.log('committed the file')
      await git.push({
        fs,
        http,
        dir: dirName,
        gitdir: repo_name,
        remote: 'origin',
        url: remote_url,
        ref: 'main',
        onAuth: () => ({ username: config.git.remote.username, password: config.git.remote.token }),
        onProgress: (message) => { console.log("progress", message) },
        onMessage: (message) => { console.log("message", message) },
        onAuthFailure: (err) => { console.log("auth failure", err) },
      })
      console.log('pushed to remote')
    })
}

function makeDir(repo, dir) {
    return new Promise((resolve, reject) => {
        const fs = new FS('/local_repo')
        let _dir = repo + dir
        fs.mkdir(_dir, {}, err => {
            !err ? resolve() : reject()
        })
    })
}